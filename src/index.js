import React from 'react';
import ReactDOM from 'react-dom';

const App = () => <h1>Good Luck!</h1>;

ReactDOM.render(<App />, document.getElementById('root'));
